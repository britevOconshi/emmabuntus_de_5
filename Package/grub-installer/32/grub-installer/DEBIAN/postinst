#! /bin/sh -e

. /usr/share/debconf/confmodule

log () {
	logger -t grub-installer "$@"
}

error () {
	log "error: $@"
}

die () {
	local template="$1"
	shift

	error "$@"
	db_input critical "$template" || [ $? -eq 30 ]
	db_go || true
	exit 1
}

mountvirtfs () {
	fstype="$1"
	path="$2"
	if grep -q "[[:space:]]$fstype\$" /proc/filesystems && \
	   ! grep -q "^[^ ]\+ \+$path " /proc/mounts; then
		mkdir -p "$path" || \
			die grub-installer/mounterr "Error creating $path"

		if mount -t "$fstype" "$fstype" "$path"; then
			log "Success mounting $path"
			trap "umount $path" HUP INT QUIT KILL PIPE TERM EXIT
		elif [ "$fstype" == "efivarfs" ]; then
			error "Error mounting $path (non-fatal)"
		else
			die grub-installer/mounterr "Error mounting $path"
		fi
	fi
}

# If we're installing grub-efi, it wants /sys mounted in the
# target. Maybe /proc too?
mountvirtfs proc /target/proc
mountvirtfs sysfs /target/sys
mountvirtfs efivarfs /target/sys/firmware/efi/efivars

grub-installer /target
